#!/usr/bin/env python3

import sys
import psutil
import time


def read_file(file_name):
    with open(file_name) as f:
        lines = f.read()
    return lines

def convert_input(input_string):
    dots, folds = input_string.split("\n\n")
    dots_set = set()
    for dot in dots.split("\n"):
        dot = dot.split(',')
        dots_set.add((int(dot[0]), int(dot[1])))

    folds_lst = []
    for fold in folds.split("\n"):
        k, v = fold.split('=')
        folds_lst.append((k.split(' ')[2], int(v)))

    return (dots_set, folds_lst)

if __name__ == '__main__':
    tic = time.perf_counter()
    arr = read_file(sys.argv[1])
    dots, folds = convert_input(arr)
    max_x = 0
    max_y = 0

    for idx, fold in enumerate(folds):
        new_dots = set()
        if fold[0] == 'x':
            coord = 0
        else: 
            coord = 1
        max_x = 0
        max_y = 0
        for dot in dots:
         
            if dot[coord] > fold[1]:
                if coord:
                    new_dot = ( dot[0], fold[1]-(dot[1]-fold[1]))
                else:
                    new_dot = ( fold[1]-(dot[0]-fold[1]), dot[1])
            else:

                new_dot = (dot)

            if new_dot[0] > max_x:
                max_x = new_dot[0]
            if new_dot[1] > max_y:
                max_y = new_dot[1]
            new_dots.add(new_dot)
    
        dots = new_dots

        if idx ==0: 
            print(f"Part 1: {len(dots)}")
            toc = time.perf_counter()
            print(f"took {toc- tic:0.12f} sec")
            
    message = [' '*max_x]*(max_y+1)
    for dot in dots:
        message[dot[1]] = message[dot[1]][:dot[0]] + \
            "#" + message[dot[1]][dot[0]+1:]
    message = "\n".join(message)
    print(f'Part 2: \n{message}')
    
    toc = time.perf_counter()
    print(f"took {toc- tic:0.12f} sec")
