#!/usr/bin/env python3
import time
def read_file():
    with open('./day1.test') as f:
        lines = f.readlines()
    
    return lines

def main():
    tic = time.perf_counter()

    arr = read_file()
    count = 0
    for i in range(len(arr)-1):
        if int(arr[i]) < int(arr[i+1]):
            count += 1
        
    print(f"Part 1: {count}")
    toc = time.perf_counter()
    print(f"took {toc- tic:0.12f} sec")
    count = 0
    for i in range(len(arr)-3):
        sum1 = int(arr[i]) + int(arr[i+1]) + int(arr[i+2])
        sum2 = int(arr[i+1]) + int(arr[i+2]) + int(arr[i+3])
        if sum1 < sum2: 
            count += 1
    
    print(f"Part 2: {count}")
    toc = time.perf_counter()
    print(f"took {toc- tic:0.12f} sec")


if __name__ == '__main__':
    main()
