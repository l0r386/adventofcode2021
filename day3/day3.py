#!/usr/bin/env python3

import sys

def read_file(file_name):
    with open(file_name) as f:
        lines = f.readlines()
        return lines

def bit_sum(arr):
    pos_count = [sum(int(line[i]) for line in arr) for i in range(len(arr[0]))]

    return pos_count

def find_o2(temp):
    for i in range(len(temp[0])):
        i_count = 0
        for line in temp:
            i_count += int(line[i])

        if i_count >= len(temp)/2:
            temp = list(filter(lambda x: x[i] == '1', temp))
        else:
            temp = list(filter(lambda x: x[i] == '0', temp))

        if len(temp) == 1:
            return temp[0]


def find_co2(temp):
    for i in range(len(temp[0])):
        i_count = 0
        for line in temp:
            i_count += int(line[i])

        if i_count < len(temp)/2:
            temp = list(filter(lambda x: x[i] == '1', temp))
        else:
            temp = list(filter(lambda x: x[i] == '0', temp))

        if len(temp) == 1:
            return temp[0]

def main():
    arr = read_file(sys.argv[1])
    arr = list(map(lambda x: x.strip(), arr))

    position_arr = bit_sum(arr)
    binary_string = ''.join(list(map(lambda x: str(int(x)), map(lambda x: x >= len(arr)//2, position_arr))))
    print(f"binary_string: {binary_string}, in dec: {int(binary_string, 2)}")
    
    o2 = int(find_o2(arr), 2)
    co2 = int(find_co2(arr), 2)

    print(f"O2: {o2}, CO2: {co2}, combined: {o2*co2}")

if __name__ == '__main__':
    main()
