#!/usr/bin/env python3

import sys
import psutil
import time
from itertools import product


HEIGHT, WIDTH = (0, 0)
def read_file(file_name):
    with open(file_name) as f:
        lines = list(map(lambda x: x.strip(), f.readlines()))
    return lines


def neigburs(x, y):
    pool = product([-1, 0, 1], repeat=2)
    neighburs_coord = []
    for coord in pool:
        if 0 <= x + coord[0] < HEIGHT and 0 <= y + coord[1] < WIDTH and not (coord[0] == 0 and coord[1] == 0):
            neighburs_coord.append((x + coord[0], y + coord[1]))
    return neighburs_coord


def update_stage(stage):
    flashes = []
    for i, row in enumerate(stage):
        for j, val in enumerate(row):
            stage[i][j] = (stage[i][j] + 1) % 10
            if stage[i][j] == 0:
                flashes.append((i, j))

    return (flashes, stage)


def step(stage):
    # first stage
    flashes_coord, stage = update_stage(stage)
    flashes = len(flashes_coord)
    # second Stage
    while flashes_coord:
        triggered_flashes = set()
        for i, j in flashes_coord:
            for ni, nj in neigburs(i, j):
                if not (stage[ni][nj] == 0):
                    stage[ni][nj] = (stage[ni][nj] + 1) % 10
                    if stage[ni][nj] == 0:
                        triggered_flashes.add((ni, nj))
                        flashes += 1
        flashes_coord = triggered_flashes

    return (stage, flashes)


if __name__ == '__main__':
    tic = time.perf_counter()
    arr = read_file(sys.argv[1])
    stage = []
    for line in arr:
        line.strip()
        row = []
        for char in line:
            row.append(int(char))

        stage.append(row)

    HEIGHT = len(stage)
    WIDTH = len(stage[0])
    summa = 0
    for i in range(1000):
        stage, flashes = step(stage)
        summa += flashes
       
        if i == 99:
            print(f"Part 1 flashes: {summa}")
            toc = time.perf_counter()
            print(f"took {toc- tic:0.12f} sec")

        if flashes == HEIGHT * WIDTH:
            print(f"Part 2 first sync flash: {i+1}")
            toc = time.perf_counter()
            print(f"took {toc- tic:0.12f} sec")
            break
        

        
   

