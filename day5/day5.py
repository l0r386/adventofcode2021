#!/usr/bin/env python3

import sys
import time

def read_file(file_name):
    with open(file_name) as f:
        lines = list(map(lambda x: x.strip(), f.readlines()))
        return lines

def create_map(raw_data, part1=False):
    
    start_points = []
    end_points = []

    for line in raw_data:
        start, end = list(line.split('->'))
        start_points.append(start.strip().split(','))
        end_points.append(end.strip().split(','))

    max_x = max([int(x[0]) for x in start_points] + [int(x[0]) for x in end_points])
    max_y = max([int(y[1]) for y in start_points] + [int(y[1]) for y in end_points])
    print(max_x)
    print(max_y)

    map_vents = []
    
    for i in range(max_x+1):
        map_vents.append(['.'] * (int(max_x)+1)) 

  
    start_points = list(map(lambda x: [int(x[0]), int(x[1])],start_points))
    end_points = list(map(lambda x: [int(x[0]), int(x[1])], end_points))

    print(start_points)

    if part1:
        valid_vents = list(filter(lambda x: x[0][0] == x[1][0] or x[0][1] == x[1][1], zip(start_points, end_points)))
       
        for vent in valid_vents:
            if vent[0][0] == vent[1][0]:
                x_cor = vent[0][0]
                
                for i in range(int(vent[0][1]), int(vent[1][1])+int(((vent[1][1] - vent[0][1]))/abs(vent[1][1] - vent[0][1])), int(((vent[1][1] - vent[0][1]))/abs(vent[1][1] - vent[0][1]))):
                    if map_vents[i][x_cor] == '.':
                        map_vents[i][x_cor] = 1
                    else:
                        map_vents[i][x_cor] += 1
            else:
                y_cor = vent[0][1]
                for i in range(int(vent[0][0]), int(vent[1][0])+int(((vent[1][0] - vent[0][0]))/abs(vent[1][0] - vent[0][0])), int(((vent[1][0] - vent[0][0]))/abs(vent[1][0] - vent[0][0]))):
                    print(i)
                 
                    if map_vents[y_cor][i] == '.':
                        map_vents[y_cor][i] = 1
                    else:
                        map_vents[y_cor][i] += 1
    else:
        valid_vents = list(filter(lambda x: x[0][0] == x[1][0] or x[0][1] == x[1][1] or abs(x[0][0]-x[1][0]) == abs(x[0][1]-x[1][1]), zip(start_points, end_points)))
        print(valid_vents)
        
        for vent in valid_vents:
            print(vent)
            if vent[0][0] == vent[1][0]:
                x_cor = vent[0][0]
                for i in range(int(vent[0][1]), int(vent[1][1])+int(((vent[1][1] - vent[0][1]))/abs(vent[1][1] - vent[0][1])), int(((vent[1][1] - vent[0][1]))/abs(vent[1][1] - vent[0][1]))):
                    if map_vents[i][x_cor] == '.':
                        map_vents[i][x_cor] = 1
                    else:
                        map_vents[i][x_cor] += 1
            elif vent[0][1] == vent[1][1]:
                y_cor = vent[0][1]
                for i in range(int(vent[0][0]), int(vent[1][0])+int(((vent[1][0] - vent[0][0]))/abs(vent[1][0] - vent[0][0])), int(((vent[1][0] - vent[0][0]))/abs(vent[1][0] - vent[0][0]))):
                    if map_vents[y_cor][i] == '.':
                        map_vents[y_cor][i] = 1
                    else:
                        map_vents[y_cor][i] += 1
            else:
                y = vent[0][1]
                for x in range(int(vent[0][0]), int(vent[1][0])+int(((vent[1][0] - vent[0][0]))/abs(vent[1][0] - vent[0][0])), int(((vent[1][0] - vent[0][0]))/abs(vent[1][0] - vent[0][0]))):
                    print(f"x: {x} y: {y}")
                    if vent[0][1] > vent[1][1] and vent[0][0] < vent[1][0]:
                        if map_vents[y][x] == '.':
                            map_vents[y][x] = 1
                        else:
                            map_vents[y][x] += 1
                        y -= 1
                    elif vent[0][1] < vent[1][1] and vent[0][0] < vent[1][0]:
                        if map_vents[y][x] == '.':
                            map_vents[y][x] = 1
                        else:
                            map_vents[y][x] += 1
                        y += 1
                    elif vent[0][1] < vent[1][1] and vent[0][0] > vent[1][0]:
                        if map_vents[y][x] == '.':
                            map_vents[y][x] = 1
                        else:
                            map_vents[y][x] += 1
                        y += 1
                    elif vent[0][1] > vent[1][1] and vent[0][0] > vent[1][0]:
                        if map_vents[y][x] == '.':
                            map_vents[y][x] = 1
                        else:
                            map_vents[y][x] += 1
                        y -= 1
            # print_map(map_vents)
            # time.sleep(3)




    # print_map(map_vents)

    print(count_dangerous_vents(map_vents))

def count_dangerous_vents(map_vents):
    sum = 0
    for line in map_vents:
        for e in line:
            if e != '.' and e !=1:
                sum += 1
    return sum

            
  
def print_map(maps):
    for line in maps:
        print(line)
             
if __name__ == '__main__':
    arr = read_file(sys.argv[1])
    create_map(arr)


