#!/usr/bin/env python3

import sys
import psutil
import time
from collections import Counter


def read_file(file_name):
    with open(file_name) as f:
        lines = f.read()
    return lines


def convert_input(input_string):
    polymer, polymer_inserts_raw = input_string.split("\n\n")
    polymer_inserts = {}
    for polymer_insert_raw in polymer_inserts_raw.split("\n"):
        k, v = polymer_insert_raw.split(" -> ")
        polymer_inserts[k] = v

    return (polymer, polymer_inserts)


if __name__ == '__main__':
    tic = time.perf_counter()
    arr = read_file(sys.argv[1])

    polymer, polymer_inserts = convert_input(arr)

    for _ in range(10):
        new_poylmer = polymer[0]
        for idx in range(1, len(polymer)):
            if polymer[idx-1:idx+1] in polymer_inserts:
                new_poylmer = new_poylmer + \
                    polymer_inserts[polymer[idx-1:idx+1]] + polymer[idx]

        polymer = new_poylmer

    values = Counter(polymer).values()

    print(f"Part 1: {max(values) -  min(values)}")

    polymer, polymer_inserts = convert_input(arr)
    counter = {}
    for k in set(polymer + ''.join(polymer_inserts.values())):
        counter[k] = 0
    for k in polymer:
        counter[k] += 1

    polymer_hash = {}
    for idx in range(1, len(polymer)):
        polymer_hash[polymer[idx-1:idx+1]] = polymer_hash[polymer[idx - 1:idx+1]] + 1 if polymer[idx-1:idx+1] in polymer_hash else 1
    

    for _ in range(40):
        new_hash = polymer_hash.copy()
        for k, v in polymer_hash.items():
            if v > 0:
                new_poly = polymer_inserts[k]
                new_hash[k] -= v
                for key in [f"{k[0]}{new_poly}", f"{new_poly}{k[1]}"]:


                    new_hash[key] = new_hash[key] + \
                        v if key in new_hash else v

                counter[new_poly] += v
            else:
                new_hash[k] += v
        polymer_hash = new_hash

    values = Counter(counter.values()).values()
    print(f"Part 2: {max(counter.values()) -  min(counter.values())}")

    toc = time.perf_counter()
    print(f"took {toc- tic:0.12f} sec")
