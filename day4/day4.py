#!/usr/bin/env python3

import sys


def read_file(file_name):
    with open(file_name) as f:
        lines = f.readlines()
        return lines

def create_table(arr):
    result = []
    for line in arr:
        if line == '':
            result.append([])
        else:        
            result[-1].append(line.split())

    return result

def mark(x, number):
    #print (f"number:{ number} element:{x}")
    if x == number:
        #print('?????')
        return 'X'
    else:
        return x

def mark_table(table, number):
    new_table = []
    for line in table:
        new_table.append([mark(x, number) for x in line])
    
    return new_table

def rotate(table):
    return list(zip(*table[::-1]))

def check_tabel_win_condition(table):
    #print("chekc win")
    #print(table)
    rotated_table = rotate(table)
    for i in range(len(table)):
        horizontal_win = all(['X' == e for e in table[i]])
        vertical_win = all(['X' == e for e in rotated_table[i]])
        #print(f"hor: {horizontal_win} ver: {vertical_win}")
        if horizontal_win or vertical_win:
            #print('???')
            return True
    return False

def calculate_score(table, number):
    sum = 0
    for line in table:
        for element in line:
            if element != 'X':
                sum += int(element)

    return sum * int(number)

def main():
    arr = read_file(sys.argv[1])
    arr = list(map(lambda x: x.strip(), arr))

    empty_line = ['Y'] *5

    drawn_numbers = arr.pop(0).split(',')
    tables = create_table(arr)
    
    number = 22
    #print([mark(x, number) for x in tables[0][0]])

    for number in drawn_numbers:
        tables = [mark_table(table, number) for table in tables]

        for table in tables:
            if check_tabel_win_condition(table):
                print(f"latest win score: {calculate_score(table, number)}, board: {tables.index(table)}, number: {number}, index: {drawn_numbers.index(number)}")
                tables[tables.index(table)] = [['Y'] * 5] * 5
    

if __name__ == '__main__':
    main()
