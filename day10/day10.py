#!/usr/bin/env python3

import sys
import psutil
import time


open_symbols = '<{[('
close_symbols = '>}])'

def read_file(file_name):
    with open(file_name) as f:
        lines = list(map(lambda x: x.strip(), f.readlines()))
    return lines

def calculate_score(list_of_close_symbol):

    table = {
        ')': 1,
        ']': 2,
        '}': 3,
        '>': 4
    }
    summa = 0
    for x in list(map(lambda x: table[x], list_of_close_symbol)):
        summa = summa *5 + x
    return summa

if __name__ == '__main__':
    tic = time.perf_counter()
    arr = read_file(sys.argv[1])
    wrongs = []
    incomplete_lines = []

    for line in arr:
        idx = 0
        targets = []
        incomplete_line = True
        while idx < len(line):
            if line[idx] in open_symbols:
                targets.append(close_symbols[open_symbols.index(line[idx])])
            else:
                if line[idx] == targets[-1]:
                    targets.pop()
                else: 
                    wrongs.append(line[idx])
                    incomplete_line = False
                    break
            
            idx += 1
        
        if incomplete_line:
            incomplete_lines.append((list(reversed(targets))))
            
    table = {
        ')': 3,
        ']': 57,
        '}': 1197,
        '>': 25137 
    }

    summa = 0
    for char in wrongs:
        summa += table[char]

    print(f"Part 1: {summa}")

    toc = time.perf_counter()
    print(f"took {toc- tic:0.12f} sec")

    print(f"Part 2: {sorted(list(map(lambda x: calculate_score(x), incomplete_lines)))[len(incomplete_lines)//2]}")
    toc = time.perf_counter()
    print(f"took {toc- tic:0.12f} sec")
