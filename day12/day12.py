#!/usr/bin/env python3

import sys
import psutil
import time


def read_file(file_name):
    with open(file_name) as f:
        lines = list(map(lambda x: x.strip(), f.readlines()))
    return lines


def part2_check(cave, path):
    if cave in path:
        if cave == 'start':
            return False
        path_list = path.split('-')

        path_list1 = list(filter(lambda x: x.islower(), path_list))

        if len(path_list1) == len(set(path_list1)):
            return True
        else:
            return False
    else:
        return True


def path_discover_part1(path, cave, cave_map):
    if cave == 'end':
        return (1, f"{path}-{cave}")

    possible_next_caves = []
    for c in cave_map[cave]:
        if c.islower():
            if c not in path:
                possible_next_caves.append(c)
        else:
            possible_next_caves.append(c)
    if len(possible_next_caves) == 0:
        return (0, f"{path}-{cave}")

    pathes = 0
    path_strings = []
    for next_cave in possible_next_caves:
        p, ps = path_discover_part1(f"{path}-{cave}", next_cave, cave_map)
        if p != 0:
            pathes += p
            path_strings.append(ps)

    return (pathes, path_strings)

def path_discover_part2(path, cave, cave_map):
    if cave == 'end':
        return (1, f"{path}-{cave}")

    possible_next_caves = []
    for c in cave_map[cave]:
        if c.islower():
            if part2_check(c, f"{path}-{cave}"):
                possible_next_caves.append(c)
        else:
            possible_next_caves.append(c)
    if len(possible_next_caves) == 0:
        return (0, f"{path}-{cave}")

    pathes = 0
    path_strings = []
    for next_cave in possible_next_caves:
        p, ps = path_discover_part2(f"{path}-{cave}", next_cave, cave_map)
        if p != 0:
            pathes += p
            path_strings.append(ps)

    return (pathes, path_strings)


if __name__ == '__main__':
    tic = time.perf_counter()
    arr = read_file(sys.argv[1])
    cave_map = {}
    for line in arr:
        start_cave, end_cave = line.split('-')
        if start_cave not in cave_map:
            cave_map[start_cave] = set()
        if end_cave not in cave_map:
            cave_map[end_cave] = set()
        cave_map[start_cave].add(end_cave)
        cave_map[end_cave].add(start_cave)

    number, pathes = path_discover_part1("", "start", cave_map)
    print(f"Part 1: {number}")

    toc = time.perf_counter()
    print(f"took {toc- tic:0.12f} sec")



    number, pathes = path_discover_part2("", "start", cave_map)
    print(f"Part 2: {number}")

    toc = time.perf_counter()
    print(f"took {toc- tic:0.12f} sec")
