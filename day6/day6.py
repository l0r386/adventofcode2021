#!/usr/bin/env python3

import sys
import os
import psutil
import time

def read_file(file_name):
    with open(file_name) as f:
        lines = list(map(lambda x: x.strip(), f.readlines()))
        return lines

def update_fish(fish):
    if fish == 0:
        fish = 6
    else:
        fish -= 1
    return fish

def original(arr):
    process = psutil.Process(os.getpid())
    for _ in range(256):
        tic = time.perf_counter()
        count = arr.count(0)
        arr = list(map(lambda x: update_fish(x), arr))
        for i in range(count):
            arr.append(8)
        toc = time.perf_counter()
        print(f"{_}. took {toc - tic: 0.4f} sec, Array length: {len(arr)}, memory usage: {process.memory_info().rss}")


    print(f"fishes: {len(arr)}")

             
if __name__ == '__main__':
    arr = read_file(sys.argv[1])

    arr = list(map(lambda x: int(x), arr[0].split(',')))
    
    # original(arr)
    fishes_counter = [0] *9

    for item in arr:
        fishes_counter[item] += 1

    tic = time.perf_counter()

    for _ in range(256):
        tmp = fishes_counter[1:]
        tmp.append(fishes_counter[0])
        tmp[6] += fishes_counter[0]
        fishes_counter = tmp
    toc = time.perf_counter()
    print(f"{_}. took {toc- tic:0.12f} sec")
    
    print(sum(fishes_counter))
    original(arr)





