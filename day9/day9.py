#!/usr/bin/env python3

import sys
import psutil
import time


def read_file(file_name):
    with open(file_name) as f:
        lines = list(map(lambda x: x.strip(), f.readlines()))
        return lines


def reshape(lst, n):
    return [lst[i*n:(i+1)*n] for i in range(len(lst)//n)]

def check_neighbour(p_coor, c_coor, flat_map, length):
    summa = 1
    if p_coor != None:
        prev = flat_map[p_coor]
    else:
        prev = None

    
    if p_coor == None or (flat_map[c_coor] > 0 and flat_map[c_coor] != 9 and flat_map[c_coor] > abs(flat_map[p_coor])):
        print(f"found {flat_map[c_coor]} at {c_coor % length}")
        for coor in [ x for x in [c_coor -1, c_coor +1, c_coor +length, c_coor - length] if 0<= x <=len (flat_map)-1 and x != p_coor]:
            flat_map[c_coor] = abs(flat_map[c_coor]) * -1 
            summa += check_neighbour(c_coor, coor, flat_map, length)
        if p_coor == None:
            for _ in reshape(flat_map, length):
                print(_)
        return summa
    else:        
        return 0



if __name__ == '__main__':
    tic = time.perf_counter()

    arr = read_file(sys.argv[1])

    flat_map = [int(item) for sublist in arr for item in sublist]
    length = len(arr[0])
    idx = 0
    summa = 0
    basin_starts = []
    for item in flat_map:
        valid_element = [x for x in [idx-1, idx+1, idx - length, idx + length] if 0 <= x <= len(flat_map)-1]
        if list(map(lambda x: item < flat_map[x], valid_element)).count(False) == 0:
            basin_starts.append(idx)
            summa += int(item) + 1

        idx += 1
        
    print(f"Part 1: {summa}")
    toc = time.perf_counter()
    print(f"took {toc- tic:0.12f} sec")



    basins = []
    for basin in basin_starts:
        basins.append(check_neighbour(None, basin, flat_map.copy(), length))
        if check_neighbour(None, basin, flat_map.copy(), length) == 107:
            exit()

    print(sorted(basins))
    summa = 1
    for _ in range(3):
        max_bas = max(basins)
        print(max_bas)
        summa *= max_bas
        basins.remove(max_bas)
    
    print(f"Part 2: {summa}")

    toc = time.perf_counter()
    print(f"took {toc- tic:0.12f} sec")
