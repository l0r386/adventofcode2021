#!/usr/bin/env python3

import sys

class Position:
    x = 0
    y = 0
    aim = 0

def read_file(file_name):
    with open(file_name) as f:
        lines = f.readlines()
        return lines


def main():
    arr = read_file(sys.argv[1])
    pos = Position()

    for line in arr:
        command, value = line.split()
        if command == 'forward':
            pos.x += int(value)
            pos.y += pos.aim * int(value)
        elif command == 'up':
            pos.aim -= int(value)
        else:
            pos.aim += int(value)
        
        print(f"Curretn pos: X:{pos.x} y:{pos.y}")

    print(pos.x * pos.y)



if __name__ == '__main__':
    main()
