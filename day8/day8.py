#!/usr/bin/env python3

import sys
import psutil
import time

def read_file(file_name):
    with open(file_name) as f:
        lines = list(map(lambda x: x.strip(), f.readlines()))
        return lines

def count_easy_digit(input):
    summa = 0
    for line in input:
        summa += len(list(filter(lambda x: len(x) in [2, 3, 4, 7], line.split('|')[1].strip().split(' '))))
    
    return summa

def find_in_string(word, pattern):
    pattern = set(pattern)
    return [x for x in word if x in pattern]


def remove_from_string(word, pattern):
    pattern = set(pattern)
    return [x for x in word if x not in pattern]

def decode_signal(line):
    cypher_key = {}
    words = sorted(
        list(map(lambda x: ''.join(sorted(x)), line.strip().split(' '))), key=len)
    cypher_key['1'] = words[0]
    cypher_key['4'] = words[2]
    cypher_key['7'] = words[1]
    cypher_key['8'] = words[-1]
    for word in list(filter(lambda x: len(x) == 6, words)):
        if len(remove_from_string(word, cypher_key['1']+cypher_key['7']+cypher_key['4'])) == 1:
            cypher_key['9'] = word
        elif len(find_in_string(word, cypher_key['1'])) == 1:
            cypher_key['6'] = word 
        else:
            cypher_key['0'] = word
    
    for word in list(filter(lambda x: len(x) == 5, words)):
        if len(remove_from_string(word, cypher_key['1'])) == 3:
            cypher_key['3'] = word
        elif len(remove_from_string(word, cypher_key['6'])) == 0:
            cypher_key ['5'] = word
        else:
            cypher_key['2'] = word

    
    return {k: v for v, k in cypher_key.items()}


if __name__ == '__main__':
    tic = time.perf_counter()
    
    arr = read_file(sys.argv[1])

    print(f"Part 1: {count_easy_digit(arr)}")

    summa  = 0
    for line in arr:
        signal, val = line.split('|')
        decoder = decode_signal(signal)
        summa += int(''.join([decoder[''.join(sorted(x))] for x in val.strip().split()]))
    
    print(f"Part 2: {summa}")

    toc = time.perf_counter()
    print(f"took {toc- tic:0.12f} sec")
    


