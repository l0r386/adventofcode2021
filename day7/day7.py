#!/usr/bin/env python3

import sys
import os
import psutil
import time
from functools import lru_cache

def read_file(file_name):
    with open(file_name) as f:
        lines = list(map(lambda x: x.strip(), f.readlines()))
        return lines


def calc_fuel(start, end, count=1):
    return sum(range(abs(start-end)+1)) * count


def calc_total_fuel_consumption(data, end):
    summma = 0
    for k, v in data.items():
        summma += calc_fuel(k, end, v)
    return summma

def find_lowest_fuel_consuption(data, length):
    tic = time.perf_counter()

    print(min([calc_total_fuel_consumption(data, idx) for idx in range(0, max(data.keys()))]))
    toc = time.perf_counter()
    print(f"took {toc- tic:0.12f} sec")

    tic = time.perf_counter()

    start = length // 2
    fuel = calc_total_fuel_consumption(data, start)

    while True:
        fuel_left = calc_total_fuel_consumption(data, start - 1)
        fuel_right = calc_total_fuel_consumption(data, start + 1)
        if fuel_left < fuel:
            fuel = fuel_left
            start = start-1
        elif fuel > fuel_right:
            fuel = fuel_right
            start = start + 1
        else:
            print(fuel)
            break
    toc = time.perf_counter()
    print(f"took {toc- tic:0.12f} sec")


if __name__ == '__main__':
    arr = read_file(sys.argv[1])

    arr = list(map(lambda x: int(x), arr[0].split(',')))

    data = {}
    for i in arr:
        if i in data.keys():
            data[i] += 1
        else:
            data[i] = 1

     
    # for i in range(0, max(arr)):
    #     summma = calc_total_fuel_consumption(data, i)
        
    #     print(f"idx: {i} sum: {summma}")
    tic = time.perf_counter()
    print(find_lowest_fuel_consuption(data, len(arr)))
    toc = time.perf_counter()
    print(f"took {toc- tic:0.12f} sec")

